package com.springboot.loc.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.loc.DTO.RegionFullDTO;
import com.springboot.loc.DTO.RegionFullUUIDDTO;
import com.springboot.loc.DTO.ServiceResponseDTO;
import com.springboot.loc.model.TbLocRegionEntity;
import com.springboot.loc.model.VwFullRegionEntity;
import com.springboot.loc.service.TbLocRegionService;

@RestController
@RequestMapping("/region")
public class TbLocRegionController {
	@Autowired
	TbLocRegionService service;
	@Autowired
	Validator validator;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> get(@RequestParam String code, @RequestParam String name) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocRegionEntity> content = service.get(code, name);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getByUUID")
	public ResponseEntity<ServiceResponseDTO> getByUUID(@RequestParam UUID regionUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocRegionEntity> content = service.getByUUID(regionUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getFull")
	public ResponseEntity<ServiceResponseDTO> getFull(@RequestParam String regionName, @RequestParam String regionCode,
			@RequestParam String countryName, @RequestParam String countryCode) {
		return this.getFullxec(regionName, regionCode, countryName, countryCode);
	}

	@PostMapping("/getFull")
	public ResponseEntity<ServiceResponseDTO> getFull(@RequestBody RegionFullDTO DTO) {
		return this.getFullxec(DTO.getCountryCode(), DTO.getCountryName(), DTO.getRegionCode(), DTO.getRegionName());
	}

	private ResponseEntity<ServiceResponseDTO> getFullxec(String regionName, String regionCode, String countryName,
			String countryCode) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<VwFullRegionEntity> content = service.getFull(regionName, regionCode, countryName, countryCode);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getFullByUUID")
	public ResponseEntity<ServiceResponseDTO> getFullByUUID(@RequestParam UUID regionUUID,
			@RequestParam UUID countryUUID) {
		return this.getFullByUUIDxec(regionUUID, countryUUID);
	}

	@PostMapping("/getFullByUUID")
	public ResponseEntity<ServiceResponseDTO> getFullByUUID(@RequestBody RegionFullUUIDDTO DTO) {
		return this.getFullByUUIDxec(DTO.getRegionUUID(), DTO.getCountryUUID());
	}

	private ResponseEntity<ServiceResponseDTO> getFullByUUIDxec(UUID regionUUID, UUID countryUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {

			List<VwFullRegionEntity> content = service.getFullByUUID(regionUUID, countryUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}
}
