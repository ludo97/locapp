package com.springboot.loc.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.loc.DTO.ProvinceFullDTO;
import com.springboot.loc.DTO.ProvinceFullUUIDDTO;
import com.springboot.loc.DTO.ServiceResponseDTO;
import com.springboot.loc.model.TbLocProvinceEntity;
import com.springboot.loc.model.VwFullProvinceEntity;
import com.springboot.loc.service.TbLocProvinceService;

@RestController
@RequestMapping("/province")
public class TbLocProvinceController {
	@Autowired
	TbLocProvinceService service;
	@Autowired
	Validator validator;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> get(@RequestParam String code, String name, String abbreviation) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocProvinceEntity> content = service.get(code, name, abbreviation);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getByUUID")
	public ResponseEntity<ServiceResponseDTO> getByUUID(@RequestParam UUID provinceUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocProvinceEntity> content = service.getByUUID(provinceUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getFull")
	public ResponseEntity<ServiceResponseDTO> getFull(@RequestParam String provinceName,
			@RequestParam String provinceCode, @RequestParam String regionName, @RequestParam String regionCode,
			@RequestParam String countryName, @RequestParam String countryCode) {
		return this.getFullxec(provinceName, provinceCode, regionName, regionCode, countryName, countryCode);
	}

	@PostMapping("/getFull")
	public ResponseEntity<ServiceResponseDTO> getFull(@RequestBody ProvinceFullDTO DTO) {
		return this.getFullxec(DTO.getProvinceCode(), DTO.getProvinceCode(), DTO.getRegionName(), DTO.getRegionCode(),
				DTO.getCountryName(), DTO.getCountryCode());
	}

	private ResponseEntity<ServiceResponseDTO> getFullxec(String provinceName, String provinceCode, String regionName,
			String regionCode, String countryName, String countryCode) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<VwFullProvinceEntity> content = service.getFull(provinceName, provinceCode, regionName, regionCode,
					countryName, countryCode);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getFullByUUID")
	public ResponseEntity<ServiceResponseDTO> getFullByUUID(@RequestParam UUID provinceUUID,
			@RequestParam UUID regionUUID, @RequestParam UUID countryUUID) {
		return this.getFullByUUIDxec(provinceUUID, regionUUID, countryUUID);
	}

	@PostMapping("/getFullByUUID")
	public ResponseEntity<ServiceResponseDTO> getFullByUUID(@RequestBody ProvinceFullUUIDDTO DTO) {
		return this.getFullByUUIDxec(DTO.getCountryUUID(), DTO.getProvinceUUID(), DTO.getRegionUUID());
	}

	private ResponseEntity<ServiceResponseDTO> getFullByUUIDxec(UUID provinceUUID, UUID regionUUID, UUID countryUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<VwFullProvinceEntity> content = service.getFullByUUID(provinceUUID, regionUUID, countryUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}
}
