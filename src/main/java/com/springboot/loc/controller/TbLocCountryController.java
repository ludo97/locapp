package com.springboot.loc.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.loc.DTO.ServiceResponseDTO;
import com.springboot.loc.model.TbLocCountryEntity;
import com.springboot.loc.service.TbLocCountryService;

@RestController
@RequestMapping("/country")
public class TbLocCountryController {
	@Autowired
	TbLocCountryService service;
	@Autowired
	Validator validator;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> get(@RequestParam String code, @RequestParam String name) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocCountryEntity> content = service.get(code, name);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getByUUID")
	public ResponseEntity<ServiceResponseDTO> getByUUID(@RequestParam UUID countryUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocCountryEntity> content = service.getByUUID(countryUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
