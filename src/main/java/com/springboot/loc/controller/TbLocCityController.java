package com.springboot.loc.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.loc.DTO.CityFullDTO;
import com.springboot.loc.DTO.CityFullUUIDDTO;
import com.springboot.loc.DTO.ServiceResponseDTO;
import com.springboot.loc.model.TbLocCityEntity;
import com.springboot.loc.model.VwFullCityEntity;
import com.springboot.loc.service.TbLocCityService;

@RestController
@RequestMapping("/city")
public class TbLocCityController {
	@Autowired
	TbLocCityService service;
	@Autowired
	Validator validator;

	@GetMapping("/get")
	public ResponseEntity<ServiceResponseDTO> get(@RequestParam String code, @RequestParam String name) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocCityEntity> content = service.get(code, name);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getByUUID")
	public ResponseEntity<ServiceResponseDTO> getByUUID(@RequestParam UUID cityUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<TbLocCityEntity> content = service.getByUUID(cityUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/getFull")
	public ResponseEntity<ServiceResponseDTO> getFull(@RequestParam String cityName, @RequestParam String cityCode,
			@RequestParam String provinceName, @RequestParam String provinceCode, @RequestParam String regionName,
			@RequestParam String regionCode, @RequestParam String countryName, @RequestParam String countryCode) {
		return this.getFullExec(cityName, cityCode, provinceName, provinceCode, regionName, regionCode, countryName,
				countryCode);
	}

	@PostMapping("/getFull")
	public ResponseEntity<ServiceResponseDTO> getFull(@RequestBody CityFullDTO DTO) {
		return this.getFullExec(DTO.getCityName(), DTO.getCityCode(), DTO.getProvinceName(), DTO.getProvinceCode(),
				DTO.getRegionName(), DTO.getRegionCode(), DTO.getCountryName(), DTO.getCountryCode());
	}

	private ResponseEntity<ServiceResponseDTO> getFullExec(String cityName, String cityCode, String provinceName,
			String provinceCode, String regionName, String regionCode, String countryName, String countryCode) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<VwFullCityEntity> content = service.getFull(cityName, cityCode, provinceName, provinceCode, regionName,
					regionCode, countryName, countryCode);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/getFullByUUID")
	public ResponseEntity<ServiceResponseDTO> getFullByUUID(@RequestBody CityFullUUIDDTO DTO) {
		return this.getFullByUUIDExec(DTO.getCityUUID(), DTO.getProvinceUUID(), DTO.getRegionUUID(),
				DTO.getCountryUUID());

	}

	@GetMapping("/getFullByUUID")
	public ResponseEntity<ServiceResponseDTO> getFullByUUID(@RequestParam UUID cityUUID,
			@RequestParam UUID provinceUUID, @RequestParam UUID regionUUID, @RequestParam UUID countryUUID) {
		return this.getFullByUUIDExec(cityUUID, provinceUUID, regionUUID, countryUUID);

	}

	private ResponseEntity<ServiceResponseDTO> getFullByUUIDExec(UUID cityUUID, UUID provinceUUID, UUID regionUUID,
			UUID countryUUID) {
		ServiceResponseDTO response = new ServiceResponseDTO();
		try {
			List<VwFullCityEntity> content = service.getFullByUUID(cityUUID, provinceUUID, regionUUID, countryUUID);
			response.setStatus(true);
			response.setContent(content);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
		}
		return new ResponseEntity<ServiceResponseDTO>(response, new HttpHeaders(), HttpStatus.OK);
	}

}
