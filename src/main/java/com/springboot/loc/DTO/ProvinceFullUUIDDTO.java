package com.springboot.loc.DTO;

import java.util.UUID;

public class ProvinceFullUUIDDTO {
	UUID provinceUUID;
	UUID regionUUID;
	UUID countryUUID;

	public UUID getProvinceUUID() {
		return provinceUUID;
	}

	public void setProvinceUUID(UUID provinceUUID) {
		this.provinceUUID = provinceUUID;
	}

	public UUID getRegionUUID() {
		return regionUUID;
	}

	public void setRegionUUID(UUID regionUUID) {
		this.regionUUID = regionUUID;
	}

	public UUID getCountryUUID() {
		return countryUUID;
	}

	public void setCountryUUID(UUID countryUUID) {
		this.countryUUID = countryUUID;
	}

}
