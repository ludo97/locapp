package com.springboot.loc.DTO;

import java.util.UUID;

public class RegionFullUUIDDTO {
	UUID regionUUID;
	UUID countryUUID;

	public UUID getRegionUUID() {
		return regionUUID;
	}

	public void setRegionUUID(UUID regionUUID) {
		this.regionUUID = regionUUID;
	}

	public UUID getCountryUUID() {
		return countryUUID;
	}

	public void setCountryUUID(UUID countryUUID) {
		this.countryUUID = countryUUID;
	}

}
