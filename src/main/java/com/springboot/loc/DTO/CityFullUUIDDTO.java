package com.springboot.loc.DTO;

import java.util.UUID;

public class CityFullUUIDDTO {
	UUID cityUUID;
	UUID provinceUUID;
	UUID regionUUID;
	UUID countryUUID;

	public UUID getCityUUID() {
		return cityUUID;
	}

	public void setCityUUID(UUID cityUUID) {
		this.cityUUID = cityUUID;
	}

	public UUID getProvinceUUID() {
		return provinceUUID;
	}

	public void setProvinceUUID(UUID provinceUUID) {
		this.provinceUUID = provinceUUID;
	}

	public UUID getRegionUUID() {
		return regionUUID;
	}

	public void setRegionUUID(UUID regionUUID) {
		this.regionUUID = regionUUID;
	}

	public UUID getCountryUUID() {
		return countryUUID;
	}

	public void setCountryUUID(UUID countryUUID) {
		this.countryUUID = countryUUID;
	}

}
