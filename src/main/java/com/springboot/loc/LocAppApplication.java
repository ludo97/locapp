package com.springboot.loc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({ "com.springboot.loc" })
@EntityScan("com.springboot.loc.model")
@EnableJpaRepositories("com.springboot.loc.repository")
public class LocAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocAppApplication.class, args);
	}

}
