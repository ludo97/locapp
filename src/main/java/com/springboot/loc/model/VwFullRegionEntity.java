package com.springboot.loc.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "vw_full_region")
public class VwFullRegionEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	 @Id
	@Column(columnDefinition = "BINARY(16)", name = "countryUUID")
	private UUID countryUUID;
	@Column(name = "countryCode")
	private String countryCode;
	@Column(name = "countryName")
	private String countryName;
	@Column(columnDefinition = "BINARY(16)", name = "regionUUID")
	private UUID regionUUID;
	@Column(name = "regionCode")
	private String regionCode;
	@Column(name = "regionName")
	private String regionName;

	public UUID getCountryUUID() {
		return countryUUID;
	}

	public void setCountryUUID(UUID countryUUID) {
		this.countryUUID = countryUUID;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public UUID getRegionUUID() {
		return regionUUID;
	}

	public void setRegionUUID(UUID regionUUID) {
		this.regionUUID = regionUUID;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

}
