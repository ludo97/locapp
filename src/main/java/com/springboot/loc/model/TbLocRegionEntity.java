package com.springboot.loc.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_loc_region")
public class TbLocRegionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "regionId")
	private Integer regionId;
	@Column(columnDefinition = "BINARY(16)", name = "regionUUID")
	private UUID regionUUID;
	@Column(columnDefinition = "Integer", name = "countryId")
	private Integer countryId;
	@Column(name = "code")
	private String code;
	@Column(name = "name")
	@NotNull
	private String name;

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public UUID getRegionUUID() {
		return regionUUID;
	}

	public void setRegionUUID(UUID regionUUID) {
		this.regionUUID = regionUUID;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
