package com.springboot.loc.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_loc_city")
public class TbLocCityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "cityId")
	private Integer cityId;
	@Column(columnDefinition = "BINARY(16)", name = "cityUUID")
	private UUID cityUUID;
	@Column(columnDefinition = "Integer", name = "provinceId")
	@NotNull
	private Integer provinceId;
	@Column(name = "name")
	@NotNull
	private String name;
	@Column(columnDefinition = "BIT", name = "chiefTown")
	@NotNull
	private Boolean chiefTown;
	@Column(name = "code")
	@NotNull
	private String code;
	@Column(name = "code103")
	private Integer code103;
	@Column(name = "code107")
	private Integer code107;
	@Column(name = "code110")
	private Integer code110;
	@Column(name = "cadastralCode")
	private String cadastralCode;

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public UUID getCityUUID() {
		return cityUUID;
	}

	public void setCityUUID(UUID cityUUID) {
		this.cityUUID = cityUUID;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getChiefTown() {
		return chiefTown;
	}

	public void setChiefTown(Boolean chiefTown) {
		this.chiefTown = chiefTown;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCode103() {
		return code103;
	}

	public void setCode103(Integer code103) {
		this.code103 = code103;
	}

	public Integer getCode107() {
		return code107;
	}

	public void setCode107(Integer code107) {
		this.code107 = code107;
	}

	public Integer getCode110() {
		return code110;
	}

	public void setCode110(Integer code110) {
		this.code110 = code110;
	}

	public String getCadastralCode() {
		return cadastralCode;
	}

	public void setCadastralCode(String cadastralCode) {
		this.cadastralCode = cadastralCode;
	}

}
