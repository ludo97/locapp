package com.springboot.loc.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_loc_Country")
public class TbLocCountryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "countryId")
	private Integer countryId;
	@Column(columnDefinition = "BINARY(16)", name = "countryUUID")
	private UUID countryUUID;
	@Column(name = "code")
	@NotNull
	private String code;
	@Column(name = "name")
	@NotNull
	private String name;

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public UUID getCountryUUID() {
		return countryUUID;
	}

	public void setCountryUUID(UUID countryUUID) {
		this.countryUUID = countryUUID;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}