package com.springboot.loc.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "vw_full_city")
public class VwFullCityEntity implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
	@Column(columnDefinition = "BINARY(16)", name = "countryUUID")
	private UUID countryUUID;
	@Column(name = "countryCode")
	private String countryCode;
	@Column(name = "countryName")
	private String countryName;
	@Column(columnDefinition = "BINARY(16)", name = "regionUUID")
	private UUID regionUUID;
	@Column(name = "regionCode")
	private String regionCode;
	@Column(name = "regionName")
	private String regionName;
	@Column(columnDefinition = "BINARY(16)", name = "provinceUUID")
	private UUID provinceUUID;
	@Column(name = "provinceName")
	private String provinceName;
	@Column(name = "provinceCode")
	private String provinceCode;
	@Column(name = "provinceUnitCode")
	private String provinceUnitCode;
	@Column(name = "provinceAbbreviation")
	private String provinceAbbreviation;
	@Column(columnDefinition = "BINARY(16)", name = "cityUUID")
	private UUID cityUUID;
	@Column(name = "cityName")
	private String cityName;
	@Column(columnDefinition = "BIT", name = "chiefTown")
	private Boolean chiefTown;
	@Column(name = "cityCode")
	private String cityCode;

	public UUID getCountryUUID() {
		return countryUUID;
	}

	public void setCountryUUID(UUID countryUUID) {
		this.countryUUID = countryUUID;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public UUID getRegionUUID() {
		return regionUUID;
	}

	public void setRegionUUID(UUID regionUUID) {
		this.regionUUID = regionUUID;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public UUID getProvinceUUID() {
		return provinceUUID;
	}

	public void setProvinceUUID(UUID provinceUUID) {
		this.provinceUUID = provinceUUID;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceUnitCode() {
		return provinceUnitCode;
	}

	public void setProvinceUnitCode(String provinceUnitCode) {
		this.provinceUnitCode = provinceUnitCode;
	}

	public String getProvinceAbbreviation() {
		return provinceAbbreviation;
	}

	public void setProvinceAbbreviation(String provinceAbbreviation) {
		this.provinceAbbreviation = provinceAbbreviation;
	}

	public UUID getCityUUID() {
		return cityUUID;
	}

	public void setCityUUID(UUID cityUUID) {
		this.cityUUID = cityUUID;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Boolean getChiefTown() {
		return chiefTown;
	}

	public void setChiefTown(Boolean chiefTown) {
		this.chiefTown = chiefTown;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

}
