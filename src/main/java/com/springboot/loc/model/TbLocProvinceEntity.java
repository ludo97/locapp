package com.springboot.loc.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Cacheable(false)
@Table(name = "tb_loc_province")
public class TbLocProvinceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "Integer", name = "provinceId")
	private Integer provinceId;
	@Column(columnDefinition = "BINARY(16)", name = "provinceUUID")
	private UUID provinceUUID;
	@Column(columnDefinition = "Integer", name = "regionId")
	private Integer regionId;
	@Column(name = "name")
	@NotNull
	private String name;
	@Column(name = "code")
	private String code;
	@Column(name = "unitCode")
	private String unitCode;
	@Column(name = "abbreviation")
	@NotNull
	private String abbreviation;

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public UUID getProvinceUUID() {
		return provinceUUID;
	}

	public void setProvinceUUID(UUID provinceUUID) {
		this.provinceUUID = provinceUUID;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

}