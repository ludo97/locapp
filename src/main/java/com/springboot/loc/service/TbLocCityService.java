package com.springboot.loc.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.loc.DTO.CityFullDTO;
import com.springboot.loc.DTO.CityFullUUIDDTO;
import com.springboot.loc.exception.RecordNotFoundException;
import com.springboot.loc.model.TbLocCityEntity;
import com.springboot.loc.model.VwFullCityEntity;
import com.springboot.loc.repository.TbLocCityRepository;

@Service
public class TbLocCityService {
	@Autowired
	TbLocCityRepository repository;

	public List<TbLocCityEntity> get(String code, String name) throws RecordNotFoundException {
		List<TbLocCityEntity> res = repository.byParam(name, code);
		return res;
	}

	public List<TbLocCityEntity> getByUUID(UUID cityUUID) throws RecordNotFoundException {
		List<TbLocCityEntity> res = repository.findByUUID(cityUUID);
		return res;
	}

	public List<VwFullCityEntity> getFull(String cityName, String cityCode, String provinceName, String provinceCode,
			String regionName, String regionCode, String countryName, String countryCode)
			throws RecordNotFoundException {
		List<VwFullCityEntity> res = repository.findByFullParam(cityName, cityCode, provinceName, provinceCode,
				regionName, regionCode, countryName, countryCode);
		return res;
	}

//	public List<VwFullCityEntity> getFull(CityFullDTO DTO) throws RecordNotFoundException {
//		List<VwFullCityEntity> res = getFull(DTO.getCityName(), DTO.getCityCode(), DTO.getProvinceName(),
//				DTO.getProvinceCode(), DTO.getRegionName(), DTO.getRegionCode(), DTO.getCountryName(),
//				DTO.getCountryCode());
//		return res;
//	}

	public List<VwFullCityEntity> getFullByUUID(UUID cityUUID, UUID provinceUUID, UUID regionUUID, UUID countryUUID)
			throws RecordNotFoundException {
		List<VwFullCityEntity> res = repository.findByFullUUID(cityUUID, provinceUUID, regionUUID, countryUUID);
		return res;
	}

//	public List<VwFullCityEntity> getFullByUUID(CityFullUUIDDTO DTO) throws RecordNotFoundException {
//		List<VwFullCityEntity> res = getFullByUUID(DTO.getCityUUID(), DTO.getCountryUUID(), DTO.getProvinceUUID(),
//				DTO.getRegionUUID());
//		return res;
//	}
}