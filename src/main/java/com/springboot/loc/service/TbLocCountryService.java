package com.springboot.loc.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.loc.exception.RecordNotFoundException;
import com.springboot.loc.model.TbLocCountryEntity;
import com.springboot.loc.repository.TbLocCountryRepository;

@Service
public class TbLocCountryService {
	@Autowired
	TbLocCountryRepository repository;

	public List<TbLocCountryEntity> get(String code, String name) throws RecordNotFoundException {
		List<TbLocCountryEntity> res = repository.byParam(name, code);
		return res;
	}

	public List<TbLocCountryEntity> getByUUID(UUID countryUUID) throws RecordNotFoundException {
		List<TbLocCountryEntity> res = repository.findByUUID(countryUUID);
		return res;
	}
}