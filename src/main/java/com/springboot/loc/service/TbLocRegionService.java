package com.springboot.loc.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.loc.DTO.RegionFullDTO;
import com.springboot.loc.DTO.RegionFullUUIDDTO;
import com.springboot.loc.exception.RecordNotFoundException;
import com.springboot.loc.model.TbLocRegionEntity;
import com.springboot.loc.model.VwFullRegionEntity;
import com.springboot.loc.repository.TbLocRegionRepository;

@Service
public class TbLocRegionService {
	@Autowired
	TbLocRegionRepository repository;

	public List<TbLocRegionEntity> get(String code, String name) throws RecordNotFoundException {
		List<TbLocRegionEntity> res = repository.byParam(name, code);
		return res;
	}

	public List<TbLocRegionEntity> getByUUID(UUID regionUUID) throws RecordNotFoundException {
		List<TbLocRegionEntity> res = repository.findByUUID(regionUUID);
		return res;
	}

//	public List<VwFullRegionEntity> getFull(RegionFullDTO DTO) throws RecordNotFoundException {
//		List<VwFullRegionEntity> res = getFull(DTO.getRegionName(), DTO.getRegionCode(), DTO.getCountryName(),
//				DTO.getCountryCode());
//		return res;
//	}

	public List<VwFullRegionEntity> getFull(String regionName, String regionCode, String countryName,
			String countryCode) throws RecordNotFoundException {
		List<VwFullRegionEntity> res = repository.findByFullParam(regionName, regionCode, countryName, countryCode);
		return res;
	}

//	public List<VwFullRegionEntity> getFullByUUID(RegionFullUUIDDTO DTO) throws RecordNotFoundException {
//		List<VwFullRegionEntity> res = getFullByUUID(DTO.getRegionUUID(), DTO.getCountryUUID());
//		return res;
//	}

	public List<VwFullRegionEntity> getFullByUUID(UUID regionUUID, UUID countryUUID) throws RecordNotFoundException {
		List<VwFullRegionEntity> res = repository.findByFullUUID(regionUUID, countryUUID);
		return res;
	}
}