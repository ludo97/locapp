package com.springboot.loc.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.loc.DTO.ProvinceFullDTO;
import com.springboot.loc.DTO.ProvinceFullUUIDDTO;
import com.springboot.loc.exception.RecordNotFoundException;
import com.springboot.loc.model.TbLocProvinceEntity;
import com.springboot.loc.model.VwFullProvinceEntity;
import com.springboot.loc.repository.TbLocProvinceRepository;

@Service
public class TbLocProvinceService {
	@Autowired
	TbLocProvinceRepository repository;

	public List<TbLocProvinceEntity> get(String abbreviation, String code, String name) throws RecordNotFoundException {
		List<TbLocProvinceEntity> res = repository.byParam(name, code, abbreviation);
		return res;
	}

	public List<TbLocProvinceEntity> getByUUID(UUID provinceUUID) throws RecordNotFoundException {
		List<TbLocProvinceEntity> res = repository.findByUUID(provinceUUID);
		return res;
	}

	public List<VwFullProvinceEntity> getFull(String provinceName, String provinceCode, String regionName,
			String regionCode, String countryName, String countryCode) throws RecordNotFoundException {
		List<VwFullProvinceEntity> res = repository.findByFullParam(provinceName, provinceCode, regionName, regionCode,
				countryName, countryCode);
		return res;
	}

//	public List<VwFullProvinceEntity> getFull(ProvinceFullDTO DTO) throws RecordNotFoundException {
//		List<VwFullProvinceEntity> res = getFull(DTO.getProvinceName(), DTO.getProvinceCode(), DTO.getRegionName(),
//				DTO.getRegionCode(), DTO.getCountryName(), DTO.getCountryCode());
//		return res;
//	}

	public List<VwFullProvinceEntity> getFullByUUID(UUID provinceUUID, UUID regionUUID, UUID countryUUID)
			throws RecordNotFoundException {
		List<VwFullProvinceEntity> res = repository.findByFullUUID(provinceUUID, regionUUID, countryUUID);
		return res;
	}

//	public List<VwFullProvinceEntity> getFullByUUID(ProvinceFullUUIDDTO DTO) throws RecordNotFoundException {
//		List<VwFullProvinceEntity> res = getFullByUUID(DTO.getProvinceUUID(), DTO.getRegionUUID(),
//				DTO.getCountryUUID());
//		return res;
//	}
}
