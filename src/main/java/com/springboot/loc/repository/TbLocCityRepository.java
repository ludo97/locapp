package com.springboot.loc.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.springboot.loc.model.TbLocCityEntity;
import com.springboot.loc.model.VwFullCityEntity;

@Repository
public interface TbLocCityRepository extends JpaRepository<TbLocCityEntity, Integer> {
	@Query(" SELECT re from TbLocCityEntity re WHERE ((:name = '' AND :code = '') OR ((:name<>'' AND name like CONCAT('%', :name, '%')) OR (:code <> '' AND code like CONCAT('%', :code, '%')))) ")
	List<TbLocCityEntity> byParam(@Param("name") String name, @Param("code") String code);

	@Query(" SELECT re from TbLocCityEntity re WHERE re.cityUUID = (:cityUUID)")
	List<TbLocCityEntity> findByUUID(@Param("cityUUID") UUID cityUUID);

	@Query(" SELECT vw from VwFullCityEntity vw WHERE ((:cityCode = '' AND :cityName = '' AND :provinceCode = '' AND :provinceName = '' AND :regionCode = '' AND :regionName = '' AND :countryCode = '' AND :countryName = '')\r\n" + 
			" OR ((:cityCode<>'' AND cityCode like CONCAT('%', :cityCode, '%'))\r\n" + 
			" OR (:cityName <> '' AND cityName like CONCAT('%', :cityName, '%'))\r\n" + 
			" OR (:provinceCode <> '' AND provinceCode like CONCAT('%', :provinceCode, '%'))\r\n" + 
			" OR (:provinceName <> '' AND provinceName like CONCAT('%', :provinceName, '%'))\r\n" + 
			" OR (:regionCode <> '' AND regionCode like CONCAT('%', :regionCode, '%'))\r\n" + 
			" OR (:regionName <> '' AND regionName like CONCAT('%', :regionName, '%'))\r\n" + 
			" OR (:countryCode <> '' AND countryCode like CONCAT('%', :countryCode, '%'))\r\n" + 
			" OR (:countryName <> '' AND countryName like CONCAT('%', :countryName, '%'))\r\n" + 
			")) ")
	List<VwFullCityEntity> findByFullParam(@Param("cityName") String cityName, @Param("cityCode") String cityCode,
			@Param("provinceName") String provinceName, @Param("provinceCode") String provinceCode, @Param("regionName") String regionName,
			@Param("regionCode") String regionCode, @Param("countryName") String countryName, @Param("countryCode") String countryCode);

	@Query(" SELECT vw from VwFullCityEntity vw WHERE ((vw.cityUUID = (:cityUUID)) or (vw.provinceUUID = (:provinceUUID)) or (vw.regionUUID = (:regionUUID)) or (vw.countryUUID = (:countryUUID))) ")
	List<VwFullCityEntity> findByFullUUID(@Param("cityUUID") UUID cityUUID, @Param("provinceUUID") UUID provinceUUID,
			@Param("regionUUID") UUID regionUUID, @Param("countryUUID") UUID countryUUID);
}
