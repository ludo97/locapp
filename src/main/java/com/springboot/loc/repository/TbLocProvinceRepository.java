package com.springboot.loc.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.loc.model.TbLocProvinceEntity;
import com.springboot.loc.model.VwFullProvinceEntity;

@Repository
public interface TbLocProvinceRepository extends JpaRepository<TbLocProvinceEntity, Integer> {
	@Query(" SELECT re from TbLocProvinceEntity re WHERE ((:name = '' AND :code = '' AND :abbreviation = '') OR ((:name<>'' AND name like CONCAT('%', :name, '%')) OR (:code <> '' AND code like CONCAT('%', :code, '%')) OR (:abbreviation <> '' AND abbreviation like CONCAT('%', :abbreviation, '%')))) ")
	List<TbLocProvinceEntity> byParam(@Param("name") String name, @Param("code") String code,
			@Param("abbreviation") String abbreviation);

	@Query(" SELECT re from TbLocProvinceEntity re WHERE re.provinceUUID = (:provinceUUID)")
	List<TbLocProvinceEntity> findByUUID(@Param("provinceUUID") UUID provinceUUID);

	@Query(" SELECT vw from VwFullProvinceEntity vw WHERE ((:provinceCode = '' AND :provinceName = '' AND :regionCode = '' AND :regionName = '' AND :countryCode = '' AND :countryName = '')\r\n" + 
			" OR ((:provinceCode <> '' AND provinceCode like CONCAT('%', :provinceCode, '%'))\r\n" + 
			" OR (:provinceName <> '' AND provinceName like CONCAT('%', :provinceName, '%'))\r\n" + 
			" OR (:regionCode <> '' AND regionCode like CONCAT('%', :regionCode, '%'))\r\n" + 
			" OR (:regionName <> '' AND regionName like CONCAT('%', :regionName, '%'))\r\n" + 
			" OR (:countryCode <> '' AND countryCode like CONCAT('%', :countryCode, '%'))\r\n" + 
			" OR (:countryName <> '' AND countryName like CONCAT('%', :countryName, '%'))\r\n" + 
			")) ")
	List<VwFullProvinceEntity> findByFullParam(@Param("provinceName") String provinceName, @Param("provinceCode") String provinceCode,
			@Param("regionName") String regionName, @Param("regionCode") String regionCode, @Param("countryName") String countryName,
			@Param("countryCode") String countryCode);

	@Query(" SELECT vw from VwFullProvinceEntity vw WHERE ((vw.provinceUUID = (:provinceUUID)) or (vw.regionUUID = (:regionUUID)) or (vw.countryUUID = (:countryUUID))) ")
	List<VwFullProvinceEntity> findByFullUUID(@Param("provinceUUID") UUID provinceUUID, @Param("regionUUID") UUID regionUUID,
			@Param("countryUUID") UUID countryUUID);
}