package com.springboot.loc.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.loc.model.TbLocRegionEntity;
import com.springboot.loc.model.VwFullRegionEntity;

@Repository
public interface TbLocRegionRepository extends JpaRepository<TbLocRegionEntity, Integer> {
	@Query(" SELECT re from TbLocRegionEntity re WHERE ((:name = '' AND :code = '') OR ((:name<>'' AND name like CONCAT('%', :name, '%')) OR (:code <> '' AND code like CONCAT('%', :code, '%')))) ")
	List<TbLocRegionEntity> byParam(@Param("name") String name, @Param("code") String code);

	@Query(" SELECT re from TbLocRegionEntity re WHERE re.regionUUID = (:regionUUID)")
	List<TbLocRegionEntity> findByUUID(@Param("regionUUID") UUID regionUUID);

	@Query(" SELECT vw from VwFullRegionEntity vw WHERE ((:regionCode = '' AND :regionName = '' AND :countryCode = '' AND :countryName = '')\r\n" + 
			" OR ((:regionCode <> '' AND regionCode like CONCAT('%', :regionCode, '%'))\r\n" + 
			" OR (:regionName <> '' AND regionName like CONCAT('%', :regionName, '%'))\r\n" + 
			" OR (:countryCode <> '' AND countryCode like CONCAT('%', :countryCode, '%'))\r\n" + 
			" OR (:countryName <> '' AND countryName like CONCAT('%', :countryName, '%'))\r\n" + 
			")) ")
	List<VwFullRegionEntity> findByFullParam(@Param("regionName") String regionName, @Param("regionCode") String regionCode,
			@Param("countryName") String countryName, @Param("countryCode") String countryCode);

	@Query(" SELECT vw from VwFullRegionEntity vw WHERE ((vw.regionUUID = (:regionUUID)) or (vw.countryUUID = (:countryUUID))) ")
	List<VwFullRegionEntity> findByFullUUID(@Param("regionUUID") UUID regionUUID, @Param("countryUUID") UUID countryUUID);
}
