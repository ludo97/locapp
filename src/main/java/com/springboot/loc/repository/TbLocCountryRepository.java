package com.springboot.loc.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.loc.model.TbLocCountryEntity;

@Repository
public interface TbLocCountryRepository extends JpaRepository<TbLocCountryEntity, Integer> {
	@Query(" SELECT re from TbLocCountryEntity re WHERE ((:name = '' AND :code = '') OR ((:name<>'' AND name like CONCAT('%', :name, '%')) OR (:code <> '' AND code like CONCAT('%', :code, '%')))) ")
	List<TbLocCountryEntity> byParam(@Param("name") String name, @Param("code") String code);

	@Query(" SELECT re from TbLocCountryEntity re WHERE re.countryUUID = (:countryUUID)")
	List<TbLocCountryEntity> findByUUID(@Param("countryUUID") UUID countryUUID);
}